Name:          google-http-java-client
Version:       1.22.0
Release:       9
Summary:       Google HTTP Client Library for Java
License:       ASL 2.0
URL:           https://github.com/google/google-http-java-client/
Source0:       https://github.com/google/google-http-java-client/archive/1.22.0/google-http-java-client-1.22.0.tar.gz

BuildRequires: maven-local mvn(com.fasterxml.jackson.core:jackson-core) mvn(com.google.code.findbugs:findbugs)
BuildRequires: mvn(com.google.code.findbugs:jsr305) mvn(com.google.code.gson:gson) mvn(com.google.guava:guava)
BuildRequires: mvn(com.google.protobuf:protobuf-java) mvn(commons-codec:commons-codec)
BuildRequires: mvn(javax.jdo:jdo2-api) mvn(junit:junit) 
BuildRequires: mvn(org.apache.httpcomponents:httpclient) mvn(org.apache.maven.plugins:maven-antrun-plugin)
BuildRequires: mvn(org.codehaus.jackson:jackson-core-asl)
BuildRequires: mvn(org.codehaus.mojo:build-helper-maven-plugin)
BuildRequires: mvn(org.sonatype.oss:oss-parent:pom:) mvn(xpp3:xpp3) protobuf-devel
Provides:      bundled(apache-commons-codec) = 1.8
BuildArch:     noarch

%description
Google HTTP Client Library for Java is a flexible, efficient, and powerful
Java library for accessing any resource on the web via HTTP.

%package findbugs
Summary:       Google HTTP Client Findbugs plugin

%description findbugs
Google APIs Client Library Findbugs custom plugin.

%package gson
Summary:       Google HTTP Client GSON extensions

%description gson
GSON extensions to the Google HTTP Client Library for Java.

%package jackson
Summary:       Google HTTP Client Jackson extensions

%description jackson
Jackson extensions to the Google HTTP Client Library for Java.

%package jackson2
Summary:      Google HTTP Client Jackson 2 extensions

%description jackson2
Jackson 2 extensions to the Google HTTP Client Library for Java.

%package jdo
Summary:       Google HTTP Client JDO extensions

%description jdo
JDO extensions to the Google HTTP Client Library for Java.

%package parent
Summary:       Google HTTP Client Parent POM

%description parent
Parent POM for the Google HTTP Client Library for Java.

%package protobuf
Summary:       Google HTTP Client Protocol Buffer extensions

%description protobuf
Protocol Buffer extensions to the Google HTTP Client Library for Java.

%package test
Summary:       Google HTTP Client Test support

%description test
Shared classes used for testing of artifacts in the
Google HTTP Client Library for Java.

%package xml
Summary:       Google HTTP Client XML extensions

%description xml
XML extensions to the Google HTTP Client Library for Java.

%package help
Summary:       Help package for google-http-java-client
Provides:      %{name}-javadoc = %{version}-%{release}
Obsoletes:     %{name}-javadoc < %{version}-%{release}

%description help
This package contains javadoc for google-http-java-client.

%prep
%autosetup -n %{name}-%{version} -p1

%pom_remove_plugin org.codehaus.mojo:clirr-maven-plugin
%pom_remove_plugin -r :animal-sniffer-maven-plugin
%pom_remove_plugin :maven-checkstyle-plugin
%pom_remove_plugin -r :maven-source-plugin
%pom_remove_plugin -r :findbugs-maven-plugin
%pom_remove_plugin -r :maven-deploy-plugin
%pom_remove_plugin :maven-release-plugin
%pom_xpath_remove "pom:plugin[pom:artifactId = 'maven-javadoc-plugin']/pom:executions"
%pom_xpath_remove -r "pom:build/pom:extensions"
%pom_disable_module google-http-client-assembly
%pom_disable_module google-http-client-appengine
%pom_disable_module google-http-client-android
%pom_disable_module samples/dailymotion-simple-cmdline-sample
%pom_disable_module samples/googleplus-simple-cmdline-sample
%pom_change_dep -r :guava-jdk5 :guava
%pom_xpath_remove "pom:dependency[pom:groupId='commons-codec']/pom:scope" google-http-client
%pom_remove_plugin org.sonatype.plugins:jarjar-maven-plugin google-http-client
%pom_remove_plugin :maven-antrun-plugin google-http-client
%pom_remove_dep com.google.android:android google-http-client
%pom_remove_plugin :maven-datanucleus-plugin
%pom_remove_plugin :maven-datanucleus-plugin google-http-client-jdo

%pom_remove_plugin com.google.protobuf.tools:maven-protoc-plugin google-http-client-protobuf
%pom_add_plugin org.codehaus.mojo:build-helper-maven-plugin:1.5 google-http-client-protobuf "
<executions>
   <execution>
      <id>add-test-source</id>
      <phase>generate-test-sources</phase>
      <goals>
         <goal>add-test-source</goal>
      </goals>
      <configuration>
         <sources>
            <source>target/generated-test-sources</source>
         </sources>
      </configuration>
   </execution>
</executions>"
%pom_add_plugin org.apache.maven.plugins:maven-antrun-plugin:1.8 google-http-client-protobuf '
<executions>
  <execution>
     <id>generate-sources</id>
     <phase>generate-sources</phase>
     <configuration>
       <target>
         <mkdir dir="target/generated-test-sources"/>
         <exec failonerror="true" executable="protoc">
            <arg value="--java_out=target/generated-test-sources"/>
            <arg value="src/test/proto/simple_proto.proto"/>
         </exec>
       </target>
     </configuration>
     <goals>
       <goal>run</goal>
     </goals>
  </execution>
</executions>'

%pom_xpath_inject "pom:plugin[pom:artifactId='maven-jar-plugin']/pom:executions" "
<execution>
 <id>default-jar</id>
 <phase>skip</phase>
</execution>" google-http-client-jdo

rm -r google-http-client/src/test/java/com/google/api/client/http/apache/ApacheHttpTransportTest.java

%build
%mvn_build -s --skip-tests

%install
%mvn_install

%files -f .mfiles-google-http-client
%doc README.md LICENSE

%files findbugs -f .mfiles-google-http-client-findbugs

%files gson -f .mfiles-google-http-client-gson

%files jackson -f .mfiles-google-http-client-jackson

%files jackson2 -f .mfiles-google-http-client-jackson2

%files jdo -f .mfiles-google-http-client-jdo

%files parent -f .mfiles-google-http-client-parent

%files protobuf -f .mfiles-google-http-client-protobuf

%files test -f .mfiles-google-http-client-test

%files xml -f .mfiles-google-http-client-xml

%files help -f .mfiles-javadoc
%doc LICENSE

%changelog
* Sat Nov 19 xu_ping <xuping33@h-partners.com> - 1.22.0-9
- Modify invalid source

* Wed Jan 27 huanghaitao <huanghaitao8@huawei.com> - 1.22.0-8
- Skip test to drop datanucleus components

* Thu Aug 27 chengzihan <chengzihan2@huaei.com> - 1.22.0-7
- the binary file /usr/bin/protoc needed by this package is currently
- provided by protobuf-devel instead of protobuf

* Wed Mar 4 2020 lingsheng <lingsheng@huawei.com> - 1.22.0-6
- Package init
